package zpo.kamil.krupa.lab8;

import java.io.*;
import java.util.concurrent.BlockingQueue;

public class Writer {
    private String mFileName;
    private static BlockingQueue<Integer> mBuffer;
    private boolean mFinished = false;
    private volatile static Writer sCurrentWriter = null;

    public Writer(String fileName, BlockingQueue<Integer> buffer) {
        mFileName = fileName;
        mBuffer = buffer;
    }

    public synchronized void read(FileReader fileReader) throws IOException, InterruptedException {
        if(sCurrentWriter == null) {
            sCurrentWriter = this;
            //System.out.println("Start czytania słowa z " + mFileName);
        }
        if(sCurrentWriter != this) {
            return;
        }

        if (mBuffer.isEmpty()) {
            Integer readCharacter = fileReader.read();
            mBuffer.put(readCharacter);
            if(readCharacter.equals(32) || readCharacter.equals(10)) {
                sCurrentWriter = null;
                //Thread.sleep(1);
                //System.out.println("Koniec czytania słowa z " + mFileName);
            }
        }
    }

    public void main() throws IOException, InterruptedException {
        try (FileReader fileReader = new FileReader(mFileName)) {
            while (fileReader.ready()) {
                read(fileReader);
            }
        }
        mFinished = true;
        sCurrentWriter = null;
    }

    public boolean isFinished() {
        return mFinished;
    }
}