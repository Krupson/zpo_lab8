package zpo.kamil.krupa.lab8;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Main {

    private static BlockingQueue<Integer> mBuffer = new ArrayBlockingQueue<>(1);

    public static void main(String[] args) throws Exception {
        Writer[] writers = new Writer[]{
                new Writer("files/plik1.txt", mBuffer),
                new Writer("files/plik2.txt", mBuffer),
                new Writer("files/plik3.txt", mBuffer)
        };

        Reader reader = new Reader("files/output.txt", mBuffer);
        reader.setWriters(writers);

        Thread threadWriter1 = new Thread(() -> {
            try {
                writers[0].main();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread threadWriter2 = new Thread(() -> {
            try {
                writers[1].main();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread threadWriter3 = new Thread(() -> {
            try {
                writers[2].main();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread threadReader = new Thread(() -> {
            try {
                reader.write();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        threadWriter1.start();
        threadWriter2.start();
        threadWriter3.start();
        threadReader.start();
    }
}
