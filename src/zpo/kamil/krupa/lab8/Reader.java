package zpo.kamil.krupa.lab8;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class Reader {
    private String mFileName;
    private static BlockingQueue<Integer> mBuffer;
    private Writer[] mWriters;


    public Reader(String fileName, BlockingQueue<Integer> buffer) {
        mFileName = fileName;
        mBuffer = buffer;
    }

    public void setWriters(Writer[] writers) {
        mWriters = writers;
    }

    public void write() throws IOException, InterruptedException {
        try (FileWriter fileWriter = new FileWriter(mFileName)) {
            mainLoop:
            while (true) {
                if (!mBuffer.isEmpty()) {
                    fileWriter.write(mBuffer.take());
                    fileWriter.flush();
                }

                if(mWriters != null) {
                    for(Writer writer : mWriters) {
                        if(!writer.isFinished()) {
                            continue mainLoop;
                        }
                    }
                    return;
                }
            }
        }
    }
}
